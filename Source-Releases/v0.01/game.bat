@echo off
title Batch RPG / DevKit Release
goto initial_launch

:: This script is run when the player first runs the game
:initial_launch
if not exist basegame.gmdi (
echo PLEASE REINSTALL "basegame.gmdi"
pause >nul
exit
)
cls
echo Hello! Welcome to TESTWORKS RPG. This is the dev-kit release, this version is used for modding and update testing only!
echo --------------
echo 1) New Character
echo 2) Load
echo 3) Quit
set /p playerinput=">"

:: If statements for the line above
if %playerinput%==1 goto new_char
if %playerinput%==2 goto load_char
if %playerinput%==3 exit
:: The line below doesn't do sh1t so ignore it
goto initial_launch

:: This script is the basic setup for users to make their char
:new_char
cls
echo What would you like your name to be
echo -------------
echo NO SPACES
set /p playerinput=">"
set playername=%playerinput%
goto setup_stats

:: Sets all the stats after creating a char
:setup_stats
set gold=100
set xp=0
set xpmax=250
set level=1
set wep=0
set wepdis=Fists
set arm=0
set armdis=None
set hp=100
goto setup_save

:: Saves the game after the players char has been made
:setup_save
(
echo %gold%
echo %xp%
echo %xpmax%
echo %level%
echo %wep%
echo %wepdis%
echo %arm%
echo %armdis%
echo %hp%
) > %playername%.sav
goto main_hub

:: This script is basically a login screen without the pass
:load_char
cls
echo LOAD CHARACTER
echo --------------
echo What was your save name?
set /p playerinput=">"
set playername=%playerinput%

:: This script searches the games folder for the save file name they just entered
if not exist %playerinput%.sav (
echo ENTER A VALID SAVE
pause >nul
goto load_char
) else < %playerinput%.sav (
set /p gold=
set /p xp=
set /p xpmax=
set /p level=
set /p wep=
set /p wepdis=
set /p arm=
set /p armdis=
set /p hp=
)
echo LOADED
pause >nul
goto main_hub

:: The main menu once you have loaded or made a char
:main_hub
cls
echo MAIN HUB
echo -----------
echo 1) Fight
echo 2) Stats n' Gear
echo 3) Save
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo ----------
echo Currently on save: %playername%
echo ----------
echo 8) Settings
echo 9) Unload Save
set /p playerinput=">"

:: If statements for the script above
if %playerinput%==1 goto fight_setup
if %playerinput%==2 goto statsngear
if %playerinput%==3 goto save
if %playerinput%==8 goto settings
if %playerinput%==9 goto initial_launch

:: The initial fight setup
:fight_setup
set enhp=50
goto fight

:: The fighting menu script (in-dev)
:fight
cls
echo IN-COMBAT
echo ---------
echo ENEMY: %enhp% HP
echo YOU: %hp% HP
echo 1) Attack
echo 2) Flee
echo.
echo.
echo.
echo.
echo.
echo.
set /p playerinput=">"

:: If statements for the script above
if %playerinput%==1 goto player_attack
if %playerinput%==2 goto flee
goto fight

:: Goes back to the main hub whilst removing 50 gold from the player
:flee
echo You've fled the area. 50g has been removed from you balance
set /a gold= %gold% - 50
goto main_hub

:: This script is run when the player attacks the enemy
:player_attack
set /a enhp= %enhp% - 5
set /a xp= %xp% + 2
if %hp% LEQ 0 goto main_hub
if %enhp% LEQ 0 goto fight_rewards
echo You've hit the enemy with 5 hit points or whatever! GOOD JOB!!!
goto en_attack

:: Script is run after the player attacks the enemy
:en_attack
set /a hp= %hp% - 7
if %hp% LEQ 0 goto main_hub
if %enhp% LEQ 0 goto fight_rewards
echo You've lost 7 hp or somin!
pause >nul
goto fight

:: Rewards the player with gold when a fight is won
:fight_rewards
echo You've earnt 25g!
if %hp% LEQ 99 set hp=100
set /a gold= %gold% + 25
echo Your new balance is: %gold%
pause >nul
goto main_hub

:: The menu script for the play viewing the saves stats/gear
:statsngear
cls
echo          STATS n' GEAR
echo ---------------------------------
echo Gold: %gold% XP: %xp%/%xpmax%
echo Health: %hp%
echo Weapon: %wepdis% Armour: %armdis%
echo ---------------------------------
pause >nul
goto main_hub

:: The script for saving ;)
:save
cls
(
echo %gold%
echo %xp%
echo %xpmax%
echo %level%
echo %wep%
echo %wepdis%
echo %arm%
echo %armdis%
echo %hp%
) > %playername%.sav
echo SAVED CHARACTER
pause >nul
goto main_hub

:: Settings menu script for the users accessibility
:settings
cls
echo GAME SETTINGS
echo -------------
echo 1) Command Console
echo 2) Mod Manager
echo 3) Display Colour
echo 4) Return
echo.
echo.
echo.
set /p playerinput=">"

:: If statements for the script above
if %playerinput%==1 goto cmdconsole
if %playerinput%==2 goto modmanager
if %playerinput%==3 goto displaycolour
if %playerinput%==4 goto main_hub

:: The games internal mod manager :)
:modmanager
cls
echo MOD MANAGER
echo -----------
echo Installed mods:
:: Lists all files in the directory that have .gmdi as their file type
dir /b /a-d *.gmdi
pause >nul
goto settings

:: This menu script allows the player to change the games text colour
:displaycolour
cls
echo DISPLAY COLOURS
echo ---------------
echo 1) Gray 2) Navy
echo 3) Blue 4) Green
echo 5) Lime 6) Red
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo --------------
echo 9) Back
echo --------------
set /p playerinput=">"

:: If statements for the script above
if %playerinput%==1 color 8
if %playerinput%==2 color 1
if %playerinput%==3 color 9
if %playerinput%==4 color 2
if %playerinput%==5 color A
if %playerinput%==6 color C
if %playerinput%==9 goto settings
goto displaycolour