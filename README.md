**What's Testworks RPG?**
Testworks rpg is a simple yet complex rpg developed and managed by Tom (NeonNation). The most exciting feature in-dev is the built in mod manager! The open-source version here is for modding and testing future updates before main release. Please make sure to backup your saves.

**Can I make a mod?**
Sure you can! All you need is a file editor and the right tools! One major things that you need to make mods is to remember to use the file type ".gmdi" which is the games custom dlc and modding file type. You can also post your mods in our discord! Link to that being added here soon.